<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Route;

//Token
Route::post('api/:version/token/user', 'api/:version.Token/getToken');
Route::post('api/:version/token/verify', 'api/:version.Token/verifyToken');

//banner
Route::get('api/:version/banner', 'api/:version.Banner/getBannerList');

//Product
Route::get('api/:version/product/hot', 'api/:version.Product/getHotProductList');
Route::get('api/:version/product/recommend', 'api/:version.Product/getRecommendProductList');
Route::get('api/:version/product/one', 'api/:version.Product/getOne');
Route::get('api/:version/product/by_category', 'api/:version.Product/getAllInCategory');
Route::get('api/:version/product/answer', 'api/:version.Product/getAnswer');
Route::get('api/:version/product/answer2', 'api/:version.Product/getAnswer2');
//Category
Route::get('api/:version/category/all', 'api/:version.Category/getAllCategories');

//Order
Route::post('api/:version/order', 'api/:version.Order/placeOrder');
//Route::get('api/:version/order/paginate', 'api/:version.Order/getSummary');
Route::post('api/:version/orderresult', 'api/:version.OrderResult/placeOrderResult');
Route::get('api/:version/orderresult', 'api/:version.OrderResult/getOrderResultList');

//Pay
Route::post('api/:version/pay/pre_order', 'api/:version.Pay/getPreOrder');
Route::post('api/:version/pay/notify', 'api/:version.Pay/receiveNotify');
Route::post('api/:version/pay/re_notify', 'api/:version.Pay/redirectNotify');
Route::post('api/:version/pay/concurrency', 'api/:version.Pay/notifyConcurrency');
Route::post('api/:version/pay/check', 'api/:version.Pay/checkOrderStatus');//查询微信端订单状态

//1.1.1
Route::post('api/:version/order1', 'api/:version.Order/placeOrder');
Route::get('api/:version/order1', 'api/:version.Order/getOrderList');
Route::post('api/:version/pay1/pre_order', 'api/:version.Pay/getPreOrder');


//用户
Route::get('api/:version/user/userinfo','api/:version.User/queryUserInfo');//查询用户信息
Route::post('api/:version/user/userinfo','api/:version.User/saveUserInfo');//保存用户信息

//商务合作
Route::post('api/:version/cooperate', 'api/:version.Cooperate/placeCooperate');