<?php

// 应用公共文件

/**
 * 通用API数据格式输出
 * @param $status   状态
 * @param string $message   信息
 * @param array $data   数据
 * @param int $httpStatus   状态码
 * @return \think\response\Json 返回输出格式JSON
 */
function show($status, $message='error', $data=[], $httpStatus=200){
    $result = [
        'status' => $status,
        'message' => $message,
        'data' => $data,
    ];
    return json( $result,$httpStatus);
}

/**
 * 随机数
 * @return  string
 */
function get_random_num(){
    /* 选择一个随机的方案 */
    //mt_srand((double) microtime() * 1000000);
    return date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
}

/**
分页
 */
/**
 * @param $list 获取总条数
 * @param $limit 获取每页显示的条数
 * @param $page 获取当前页数
 * @return array
 */
function pagedata($list, $limit, $page){
    $count = count($list);
    //计算出从那条开始查询
    $tol=($page-1)*$limit+1;
    // 查询出当前页数显示的数据
    $list = User::where("id",">=","$tol")->limit("$limit")->select()->toArray();
    //返回数据
    return ["code"=>"0","msg"=>"","count"=>$count,"data"=>$list];
}