<?php

namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;

class Base extends Model{


    use SoftDelete;
  
  	//基础查询时间
	protected function base($query){
	    $query->whereNull('delete_time');// 添加软删除条件
	}
    //protected function  prefixImgUrl($value){
    //    return config('setting.img_prefix').$value;
   // }
  
   	protected $autoWriteTimestamp = true;

    public function queryDataByID($id){
        return self::where('id',$id)->find();
    }

    //['sort'=>'asc', 'id' => 'DESC']
    public function queryData($where,$order){
        return self::where($where)->order($order)->paginate();
    }
    public function queryDataNone(){
        return self::paginate();
    }
    public function queryDataByWhere($where,$order){
        return self::where($where)->order($order)->paginate();
    }
    public function queryDataOrder($where,$order){
        return self::where($where)->order($order)->paginate();
    }
    public function saveData($data, $isUpdate){
        return self::allowField(true)->isUpdate($isUpdate)->save($data);
    }
  
    //根据id软删除
    public function delDataByID($id){
      return self::destroy($id);
     // return $this->where(['id'=>$id])->delete();
    }
}