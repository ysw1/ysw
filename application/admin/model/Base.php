<?php
/**
 * Created by WXCH
 * Date: 2020/1/18 0018 11:33
 */

namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;

class Base extends Model{

    //每页条数
    protected $limit = 10;

    use SoftDelete;

    protected $autoWriteTimestamp = true;
    protected $deleteTime = 'delete_time';

    protected $order = ["id" => "desc"];

    //基础查询时间
    protected function base($query){
        $query->whereNull('delete_time');// 添加软删除条件
    }

    public function saveData($data, $isUpdate){
        return self::allowField(true)->isUpdate($isUpdate)->save($data);
    }

    public function getList($where=array()){
        return $list = self::where($where)->order($this->order)->paginate($this->limit, false);
    }

    public function queryData($where=array()){
        return self::where($where)->select();
    }

    //根据id软删除
    public function delDataByID($id){
        return self::destroy($id);
    }














    public function findDataByID($id){
        return self::where('id',$id)->find();
    }



    public function queryDataSort($order){
        return self::order($order)->paginate();
    }
    //public function queryData($where,$order){
    //    return self::where($where)->order($order)->paginate();
    //}
    public function queryDataNone(){
        return self::paginate();
    }
    public function queryDataByWhere($where,$order){
        return self::where($where)->order($order)->paginate();
    }
    public function queryDataOrder($where,$order){
        return self::where($where)->order($order)->paginate();
    }





}