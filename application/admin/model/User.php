<?php
/**
 * Created by WXCH
 * Date: 2020/1/18 0018 11:32
 */

namespace app\admin\model;

class User extends Base{

    public function getDataList($page=1){
        $start = $this->limit * ($page - 1);
        return self::limit($start,$this->limit)->order('id desc')->select();
    }
}