<?php
/**
 * Created by WXCH
 * Date: 2020/1/18 0018 11:32
 */

namespace app\admin\model;

class Order extends Base{

    public function user(){
        return $this->belongsTo('User', 'user_id', 'id');
    }

    public function product(){
        return $this->belongsTo('Product', 'product_id', 'id');
    }
    public function getOrderList(){
        return $list = self::with(['user','product'])->order($this->order)->paginate($this->limit, false);
    }
}