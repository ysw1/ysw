<?php
/**
 * Created by WXCH
 * Date: 2020/1/18 0018 11:32
 */

namespace app\admin\model;

class Answer extends Base{

    public function queryDataSelect($page=1,$where=array()){
        $start = $this->limit * ($page - 1);
        return self::limit($start,$this->limit)->where($where)->order('id desc')->select();
    }

}