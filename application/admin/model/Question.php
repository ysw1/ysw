<?php
/**
 * Created by WXCH
 * Date: 2020/1/18 0018 11:32
 */

namespace app\admin\model;

class Question extends Base{

    public function options(){
        return $this->hasMany('Option',"question_id", "id");
    }

    public function product(){
        return $this->belongsTo('Product','product_id');
    }

    public function getOneByID($id){
        //return self::where('product_id',$product_id)->find();
        return self::with(['options'])->find($id);
    }

}