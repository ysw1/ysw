<?php
/**
 * Created by WXCH
 * Date: 2020/1/18 0018 11:32
 */

namespace app\admin\model;

class Product extends Base{

    protected $order = ["sort" => "desc", 'id' => 'desc'];

    public function category(){
        return $this->belongsTo('Category','category_id');
    }

    public function getProductListAll(){
        return self::with(['category'])->order($this->order)->select();
    }

    public function getProductList(){
        return $list = self::with(['category'])->order($this->order)->paginate($this->limit, false);
    }

}