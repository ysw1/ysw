<?php
/**
 * Created by WXCH
 * Date: 2020/1/18 0018 11:32
 */

namespace app\admin\model;

class Banner extends Base{

    public function product(){
        return $this->belongsTo('Product','product_id');
    }
    public function getBannerList(){
        return $list = self::with(['product'])->order($this->order)->paginate($this->limit, false);
    }
}