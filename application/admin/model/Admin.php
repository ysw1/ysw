<?php
namespace app\admin\model;

use think\Model;

class Admin extends Model{

    /**
     * 根据用户名获取管理员数据
     * @param $username
     * @return array|bool|false|\PDOStatement|string|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getAdminByUsername($username){
        if(empty($username)){
            return false;
        }

        $where = [
            "username" => trim($username),
        ];

        $result = $this->where($where)->find();
        return $result;
    }

    /**
     * 根据主键ID更新数据
     * @param $id
     * @param $data
     * @return bool|false|int
     */
    public function updateAdminById($id, $data){
        $id = intval($id);
        if(empty($id) || empty($data) || !is_array($data)){
            return false;
        }

        $where = [
            "id" => $id
        ];
        return self::allowField(true)->isUpdate(true)->save($data, $where);
    }

}
