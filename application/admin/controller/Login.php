<?php
namespace app\admin\controller;

use app\admin\validate\Admin as AdminValidate;
use think\Controller;

class Login extends Controller{

    public function index(){
        return $this->fetch();
    }

    public function check(){
        if(!request()->isPost()) {
            return show(config("status.error"), "请求方式错误!");
        }
        $username = $this->request->param("username", "", "trim");
        $password = $this->request->param("password", "", "trim");
        $captcha = $this->request->param("captcha", "", "trim");
        $data = [
            "username" => $username,
            "password" => $password,
            "captcha" => $captcha,
        ];
        $adminValidate = new AdminValidate();
        if(!$adminValidate->check($data)){
            return show(config("status.error"), $adminValidate->getError());
        }

        try{
            $result = \app\admin\business\Admin::login($data);
        }catch (\Exception $ex){
            return show(config("status.error"), $ex->getMessage());
        }
        if($result){
            return show(config("status.success"), "登录成功!");
        }
        return show(config("status.error"), "登录失败!");
    }
  
  	public function logout(){
        session(config("session_admin"), null);
        return redirect(url("login/index"));
    }
}

