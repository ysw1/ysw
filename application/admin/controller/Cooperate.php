<?php
namespace app\admin\controller;

use app\admin\model\Cooperate as CooperateModel;

class Cooperate extends Base{

    protected $model;

    public function __construct(CooperateModel $model){
        parent::__construct();
        $this->model = $model;
    }

    public function lst(){

        $list = $this->model->getList();
        $result = $list->toArray();
        $this->assign('list', $result);
        return $this->fetch();
    }

}




