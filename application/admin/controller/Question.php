<?php
namespace app\admin\controller;

use app\admin\model\Option as OptionModel;
use app\admin\model\Question as QuestionModel;

class Question extends Base{

    protected $model;
    protected $mOptionModel;

    public function __construct(QuestionModel $model, OptionModel $mOptionModel){
        parent::__construct();
        $this->model = $model;
        $this->mOptionModel = $mOptionModel;
    }

    public function lst(){
        $product_id = input('product_id');//商品id
        $this->assign('product_id', $product_id);

        $where['product_id'] = $product_id;
        $list = $this->model->getList($where);
        $result = $list->toArray();
        $this->assign('list', $result);
        return $this->fetch();
    }

    public function add(){
        if(request()->isPost()){
            $data = request()->param();
            // 开启一个事务
            //$this->model->startTrans();
            try {
                $res = $this->model->allowField(true)->isUpdate(false)->save($data);
                //$res = $this->model->saveData($data,false);
                if($res){
                    $question_id = $this->model->id;
                    $optionData = [];
                    for($i = 0; $i < count($data['content']); $i++){
                        $optionData[] = ['question_id'=>$question_id, 'content'=>$data['content'][$i], 'score'=>$data['score'][$i]];
                    }
                    $res2 = $this->model->options()->saveAll($optionData);// 批量增加关联数据
                    if ($res2) {
                        // 事务提交
                        //$this->model->commit();
                        return show(0, '保存成功!');
                    }
                }
            }catch (\think\Exception $e) {
                // 记录日志 untodo
                // 事务回滚
                //$this->model->rollback();
                return show(1, '保存失败!');
            }
        }

        $product_id = input('product_id');//商品id
        $this->assign('product_id', $product_id);

        return $this->fetch();
    }

    public function edit(){
        if(request()->isPost()){
            $data = request()->param();
            try {
                $res = $this->model->saveData($data,true);
                if($res){
                    $question_id = $data['id'];
                    $optionData[] = array();
                    for($i = 0; $i < count($data['content']); $i++){
                        if(isset($data['idtemp'][$i])){
                            $optionData[] = ['id'=>$data['idtemp'][$i], 'question_id'=>$question_id, 'content'=>$data['content'][$i], 'score'=>$data['score'][$i]];
                        }else{
                            $optionData[] = ['question_id'=>$question_id, 'content'=>$data['content'][$i], 'score'=>$data['score'][$i]];
                        }
                    }
                    $res = $this->mOptionModel->saveAll($optionData);
                    if ($res) {
                        // 事务提交
                        //$this->model->commit();
                        return show(0, '保存成功!');
                    }
                }
            }catch (\think\Exception $e) {
                return show(1, '保存失败!');
            }
        }

        $product_id = input('product_id');//商品id
        $this->assign('product_id', $product_id);

        $id = input('id');//题目id
        $this->assign('id', $id);
        $data = $this->model->getOneByID($id);
        $this->assign('data',$data);
        return $this->fetch();
    }

    public function del(){
        $id = input('id');
        //删除
        $res = $this->model->delDataByID($id);
        if($res){
            return show(0,'删除成功!');
        }else{
            return show(1,'删除失败!');
        }
    }

}




