<?php
namespace app\admin\controller;

use app\admin\model\Banner as BannerModel;
use app\admin\model\Product as ProductModel;

class Banner extends Base{

    protected $model;
    protected $mProductModel;

    public function __construct(BannerModel $model, ProductModel $mProductModel){
        parent::__construct();
        $this->model = $model;
        $this->mProductModel = $mProductModel;
    }

    public function lst(){
        $list = $this->model->getBannerList();
        $result = $list->toArray();
        $this->assign('list', $result);
        return $this->fetch();
    }

    public function add(){
        if(request()->isPost()){
            $data = request()->param();
            if($this->model->saveData($data,false)){
                return show(0,'保存成功!');
            }else{
                return show(1,'保存失败!');
            }
        }

        $productData = $this->mProductModel->getProductListAll();
        $this->assign('productData',$productData);
        return $this->fetch();
    }
    public function edit(){
        if(request()->isPost()){
            $data = request()->param();
            if($this->model->saveData($data,true)){
                return show(0,'保存成功!');
            }else{
                return show(1,'保存失败!');
            }
        }
        $id = input('id');
        $data = $this->model->findDataByID($id);
        $this->assign('data',$data);

        $productData = $this->mProductModel->getProductListAll();
        $this->assign('productData',$productData);
        return $this->fetch();
    }

    public function del(){
        $id = input('id');
        //删除
        $res = $this->model->delDataByID($id);
        if($res){
            return show(0,'删除成功!');
        }else{
            return show(1,'删除失败!');
        }
    }

    public function status(){
        $id = input('get.id');
        $status = input('get.status');
        $res = $this->model->saveData(['id'=>$id,'status'=>$status],true);
        if($res){
            return show(0,'设置成功!');
        }else{
            return show(1,'设置失败!');
        }
    }

}

