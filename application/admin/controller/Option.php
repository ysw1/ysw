<?php
namespace app\admin\controller;

use app\admin\model\Option as OptionModel;

class Option extends Base{

    protected $model;

    public function __construct(OptionModel $model){
        parent::__construct();
        $this->model = $model;
    }

    public function del(){
        $id = input('id');
        //删除
        $res = $this->model->delDataByID($id);
        if($res){
            return show(0,'删除成功!');
        }else{
            return show(1,'删除失败!');
        }
    }

}




