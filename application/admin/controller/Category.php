<?php
namespace app\admin\controller;

use app\admin\model\Category as CategoryModel;

class Category extends Base{

    protected $model;

    public function __construct(CategoryModel $model){
        parent::__construct();
        $this->model = $model;
    }

    public function lst(){
        $list = $this->model->getList();
        $result = $list->toArray();
        $this->assign('list', $result);
        return $this->fetch();
    }

    public function add(){
        if(request()->isPost()){
            $data = request()->param();
            if($this->model->saveData($data,false)){
                return show(0,'保存成功!');
            }else{
                return show(1,'保存失败!');
            }
        }
        return $this->fetch();
    }
    public function edit(){
        if(request()->isPost()){
            $data = request()->param();
            if($this->model->saveData($data,true)){
                return show(0,'保存成功!');
            }else{
                return show(1,'保存失败!');
            }
        }
        $id = input('id');
        $data = $this->model->findDataByID($id);
        $this->assign('data',$data);
        return $this->fetch();
    }

    public function del(){
        $id = input('id');
        //删除
        $res = $this->model->delDataByID($id);
        if($res){
            return show(0,'删除成功!');
        }else{
            return show(1,'删除失败!');
        }
    }

    public function sort(){
        $id = input('get.id');
        $sort = input('get.sort');
        $res = $this->model->saveData(['id'=>$id,'sort'=>$sort],true);
        if($res){
            return show(0,'设置成功!');
        }else{
            return show(1,'设置失败!');
        }
    }

    public function status(){
        $id = input('get.id');
        $status = input('get.status');
        $res = $this->model->saveData(['id'=>$id,'status'=>$status],true);
        if($res){
            return show(0,'设置成功!');
        }else{
            return show(1,'设置失败!');
        }
    }

}

