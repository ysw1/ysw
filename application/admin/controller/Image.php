<?php
namespace app\admin\controller;

use think\Controller;
use think\Request;

class Image extends Controller{

    //上传文件
    public function upload(){
        $file = Request::instance()->file('file');
        if ($file){
            $path = ROOT_PATH . 'public' . DS . 'upload' . DS . 'image';
            $info = $file->move($path);
            if ($info && $info->getPathname()) {
                //$sub_dir = get_random_num();//获取随机的id
                // 成功上传后 获取上传信息
                $image = config('setting.image_prefix') . $info->getSaveName();
                //return show(1, 'success', [ 'id'=>$sub_dir, 'name'=>$info->getFilename(), 'path'=>$image]);
                return show(1, 'success', [ 'path'=>$image]);
            }
        }
        return show(0, 'upload error');
    }

    // layui上传图片
    public function uploadEditor()
    {
        if ($this->request->isPost()){
            $res['code'] = 0;
            $res['msg']  = "上传成功";
            // 获取表单上传文件
            $file = $this->request->file('file');
            $info = $file->move(ROOT_PATH . 'public' . DS . 'upload' . DS . 'image');//保存路径
            if ($info){
                $res['data']['title']= $info->getFilename();
                $filepath =$info->getSaveName();
                $res['data']['src'] = config('setting.domain_prefix') . config('setting.image_prefix') . $filepath;
            }else{
                $res['code'] = 1;
                $res['msg'] = '上传失败'.$file->getError();
            }
            return $res;
        }
    }
}
