<?php
namespace app\admin\controller;

use app\admin\model\Order as OrderModel;

class Order extends Base{

    protected $model;

    public function __construct(OrderModel $model){
        parent::__construct();
        $this->model = $model;
    }

    public function lst(){
        $list = $this->model->getOrderList();
        $result = $list->toArray();
        $this->assign('list', $result);
        return $this->fetch();
    }
}




