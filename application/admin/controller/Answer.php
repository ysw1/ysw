<?php
namespace app\admin\controller;

use app\admin\model\Answer as AnswerModel;

class Answer extends Base{

    protected $model;

    public function __construct(AnswerModel $model){
        parent::__construct();
        $this->model = $model;
    }

    public function lst(){
        $product_id = input('product_id');//商品id
        $this->assign('product_id', $product_id);

        $where['product_id'] = $product_id;
        $list = $this->model->getList($where);
        $result = $list->toArray();
        $this->assign('list', $result);
        return $this->fetch();
    }

    public function add(){
        if(request()->isPost()){
            $data = request()->param();
            if($this->model->saveData($data,false)) {
                return show(0, '保存成功!');
            } else {
                return show(2, '保存失败!');
            }
        }

        $product_id = input('product_id');//商品id
        $this->assign('product_id', $product_id);

        return $this->fetch();
    }
    public function edit(){
        if(request()->isPost()){
            $data = request()->param();
            if($this->model->saveData($data,true)){
                return show(0, '保存成功!');
            }else{
                return show(1, '保存失败!');
            }
        }
        $product_id = input('product_id');
        $id = input('id');
        $this->assign('product_id',$product_id);
        $this->assign('id',$id);

        $data = $this->model->findDataByID($id);
        $this->assign('data',$data);
        return $this->fetch();
    }
    public function del(){
        $id = input('id');
        //删除
        $res = $this->model->delDataByID($id);
        if($res){
            return show(0,'删除成功!');
        }else{
            return show(1,'删除失败!');
        }
    }
}




