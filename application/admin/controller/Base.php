<?php

namespace app\admin\controller;

use think\Controller;

class Base extends Controller{

    public $admin = null;

    public function _initialize(){
        // 监听
        parent::_initialize();
        if($this->isLogin()){
            return $this->redirect(url("login/index"), 302);
        }
    }

    /**
     * 判断是否登录
     * @return bool
     */
    public function isLogin(){
        $this->admin = session(config("session_admin"));

        if(empty($this->admin) || is_null($this->admin)){
            return true;
        }
        return false;
    }

}