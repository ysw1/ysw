<?php
namespace app\admin\controller;

use app\admin\model\User as UserModel;

class User extends Base{

    protected $model;

    public function __construct(UserModel $model){
        parent::__construct();
        $this->model = $model;
    }

    public function lst(){

        $list = $this->model->getList();
        $result = $list->toArray();
        $this->assign('list', $result);
        return $this->fetch();
    }

}




