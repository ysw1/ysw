<?php
namespace app\admin\controller;

use app\admin\model\Category as CategoryModel;
use app\admin\model\Product as ProductModel;

class Product extends Base{

    protected $model;
    protected $categoryModel;

    public function __construct(ProductModel $model, CategoryModel $categoryModel){
        parent::__construct();
        $this->model = $model;
        $this->categoryModel = $categoryModel;
    }

    public function lst(){

        $categoryData = $this->categoryModel->queryData(['status'=>1]);
        $this->assign('categoryData', $categoryData);

        $list = $this->model->getProductList();
        $result = $list->toArray();
        $this->assign('list', $result);
       // $result = $list->toArray();
        //echo $this->model->getLastSql();exit;
        //echo halt($list);exit;
        return $this->fetch();
    }

    public function add(){
        if(request()->isPost()){
            $data = request()->param();
            if($this->model->saveData($data,false)){
                return show(0,'保存成功!');
            }else{
                return show(1,'保存失败!');
            }
        }

        $categoryData = $this->categoryModel->queryData(['status'=>1]);
        $this->assign('categoryData', $categoryData);
        return $this->fetch();
    }
    public function edit(){
        if(request()->isPost()){
            $data = request()->param();
            if($this->model->saveData($data,true)){
                return show(0,'保存成功!');
            }else{
                return show(1,'保存失败!');
            }
        }
        $id = input('id');
        $data = $this->model->findDataByID($id);
        $this->assign('data',$data);

        $categoryData = $this->categoryModel->queryData(['status'=>1]);
        $this->assign('categoryData', $categoryData);
        return $this->fetch();
    }

    public function del(){
        $id = input('id');
        //删除
        $res = $this->model->delDataByID($id);
        if($res){
            return show(0,'删除成功!');
        }else{
            return show(1,'删除失败!');
        }
    }

    public function sort(){
        $id = input('get.id');
        $sort = input('get.sort');
        $res = $this->model->saveData(['id'=>$id,'sort'=>$sort],true);
        if($res){
            return show(0,'设置成功!');
        }else{
            return show(1,'设置失败!');
        }
    }

    public function recommend(){
        $id = input('get.id');
        $recommend = input('get.recommend');
        $res = $this->model->saveData(['id'=>$id,'is_recommend'=>$recommend],true);
        if($res){
            return show(0,'设置成功!');
        }else{
            return show(1,'设置失败!');
        }
    }

    public function hot(){
        $id = input('get.id');
        $hot = input('get.hot');
        $res = $this->model->saveData(['id'=>$id,'is_hot'=>$hot],true);
        if($res){
            return show(0,'设置成功!');
        }else{
            return show(1,'设置失败!');
        }
    }

    public function status(){
        $id = input('get.id');
        $status = input('get.status');
        $res = $this->model->saveData(['id'=>$id,'status'=>$status],true);
        if($res){
            return show(0,'设置成功!');
        }else{
            return show(1,'设置失败!');
        }
    }
}




