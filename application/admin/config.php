<?php
// 视图输出字符串内容替换
return [

    'view_replace_str'  =>  [
        '__PUBLIC__'=>'/static/admin/layuimini',
    ],

    'template'               => [
        // 模板后缀
        'view_suffix'  => 'html',
    ],

    //分页配置
    'paginate'               => [
        'type'      => 'bootstrap',
        'var_page'  => 'page',
        'list_rows' => 10,
    ],

    'session_admin' => "psyduotidouadmin",


];