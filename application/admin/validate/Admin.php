<?php
/**
 * Created by WXCH.
 * Date: 2020/1/7 0007
 * Time: 18:11
 */

namespace app\admin\validate;

use think\Validate;

class Admin extends Validate
{
    protected $rule = [
        'username' => 'require',
        'password' => 'require',
        'captcha' => 'require|length:5|checkCaptcha',
    ];

    protected $message = [
        'username.require' => '名称必须填写',
        'password.require'   => '密码必须填写',
        'captcha.require'  => '验证码必须填写',
        'captcha.length'  => '验证码填写长度不对',
    ];

    protected function checkCaptcha($value, $rule, $data=[])
    {
        if(!captcha_check($value)){
            return "您输入的验证码不正确";
        }
        return true;
    }


}