<?php
/**
 * 存放业务状态码配置文件
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/1/6 0006
 * Time: 11:29
 */

return [
    'success'                    => 1,       //成功
    'error'                     => 0,       //错误
    'not_login'                 => -1,      //未登录
    'user_is_register'          => -2,      //已经注册过
    'action_not_found'          => -3,      //方法找不到
    'controller_not_found'      => -4,      //控制器找不到

    "mysql" => [
        "table_normal" => 1,//正常
        "table_pedding" => 0,//待审核
        "table_delete" => 99,//删除
    ],
];