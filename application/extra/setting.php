<?php

return [
    'domain_prefix' => 'https://psychology.laytonacademy.com',
    'image_prefix' => '/upload/image/',
    'token_expire_in' => 7200
];
