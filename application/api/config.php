<?php
//配置文件
return [
    // 默认输出类型
    'default_return_type'    => 'json',
// 路由使用完整匹配
    'route_complete_match'   => true,

// 视图输出字符串内容替换
    'template'               => [
        // 默认模板渲染规则 1 解析为小写+下划线 2 全部转换小写
        'auto_rule'    => 2,
    ],
];