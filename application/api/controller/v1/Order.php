<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:32
 */

namespace app\api\controller\v1;

use app\api\controller\BaseController;
use app\api\service\Order as OrderService;
use app\api\service\Token;
use app\api\validate\OrderValidate;


class Order extends BaseController{

    /**
     * 下单
     * @url /order
     * @HTTP POST
     */
    public function placeOrder(){
        (new OrderValidate())->goCheck();
        $product_id = input('post.product_id');
        $uid = Token::getCurrentUid();
        $order = new OrderService();
        $status = $order->place($uid, $product_id);
        return $status;
    }

}