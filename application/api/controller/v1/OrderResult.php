<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:32
 */

namespace app\api\controller\v1;

use app\api\controller\BaseController;
use app\api\model\Order as OrderModel;
use app\api\model\OrderResult as OrderResultModel;
use app\api\service\Token;


class OrderResult extends BaseController{

    public function placeOrderResult(){
        $order_no = input('post.order_no');
        $orderData = OrderModel::where(['order_no'=>$order_no])->find();
        $res = OrderResultModel::where('order_no',$order_no)->find();
        if(!$res){
            $mOrderResultModel = new OrderResultModel();
            $mOrderResultModel->order_id = $orderData['id'];
            $mOrderResultModel->order_no = $orderData['order_no'];
            $mOrderResultModel->user_id = $orderData['user_id'];
            $mOrderResultModel->product_id = $orderData['product_id'];
            $mOrderResultModel->price = $orderData['price'];
            $mOrderResultModel->statustest = 1;
            $mOrderResultModel->snap_name = $orderData['snap_name'];
            $mOrderResultModel->snap_thumb = $orderData['snap_thumb'];
            $mOrderResultModel->snap_fakenum = $orderData['snap_fakenum'];
            $mOrderResultModel->snap_content = $orderData['snap_content'];
            $mOrderResultModel->snap_items = $orderData['snap_items'];
            $mOrderResultModel->save();
        }
    }

    public function getOrderResultList(){
        $uid = Token::getCurrentUid();
        //全部订单
        //$statustest = 0;
        //$allOrderList = OrderResultModel::getOrderResultList($uid,$statustest);
        //待付
        $statustest = 1;
        $daiOrderList = OrderResultModel::getOrderResultList($uid,$statustest);

        //已付
        $statustest = 2;
        $overOrderList = OrderResultModel::getOrderResultList($uid,$statustest);
        return [
           // 'allList' => $allOrderList,
            'daiList' => $daiOrderList,
            'overList' => $overOrderList
        ];
    }


}