<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:32
 */

namespace app\api\controller\v1;

use app\api\service\Cooperate as CooperateService;
use app\api\service\Token;
use think\Controller;

class Cooperate extends Controller{

    /**
     * 提交商务合作信息
     * @url /order
     * @HTTP POST
     */
    public function placeCooperate(){
        $cooperateInfo = input('post.cooperateInfo/a');
        $uid = Token::getCurrentUid();
        $cooperate = new CooperateService();
        return $cooperate->place($uid, $cooperateInfo);
    }

}