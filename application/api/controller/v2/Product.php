<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:32
 */

namespace app\api\controller\v2;

use app\api\controller\BaseController;
use app\api\model\Answer as AnswerModel;
use app\api\model\Order as OrderModel;
use app\api\model\Product as ProductModel;


use app\lib\exception\MissException;
use app\lib\exception\ThemeException;



class Product extends BaseController{


    public function getOne(){
        $id = input('get.id');
        $one = ProductModel::getOne($id);
        if (!$one ) {
            throw new MissException([
                'msg' => '请求商品不存在',
                'errorCode' => 40000
            ]);
        }
        return $one;
    }


    public function getHotProductList(){
        $hotProductList = ProductModel::getHotProductList();
        if (!$hotProductList ) {
            throw new MissException([
                'msg' => '请求推荐不存在',
                'errorCode' => 40000
            ]);
        }
        return $hotProductList;
    }

    public function getRecommendProductList(){
        $recommendProductList = ProductModel::getRecommendProductList();
        if (!$recommendProductList ) {
            throw new MissException([
                'msg' => '请求热门不存在',
                'errorCode' => 40000
            ]);
        }
        return $recommendProductList;
    }

    /**
     * 获取某分类下全部商品(不分页）
     * @url /product/all?id=:category_id
     * @param int $id 分类id号
     * @return \think\Paginator
     * @throws ThemeException
     */
    public function getAllInCategory($id = -1){
        //(new IDMustBePositiveInt())->goCheck();
        $products = ProductModel::getProductsByCategoryID($id, false);
        if ($products->isEmpty()) {
            throw new ThemeException();
        }
        $data = $products->toArray();
        return $data;
    }


    public function getAnswer(){
        $id = input('get.id');//订单id
        $product_id = input('get.product_id');//商品id
        $score = input('get.score');//分值
        //1、获取答案
        $answer = AnswerModel::where('product_id',$product_id)->where('minscore','elt',$score)->where('maxscore','egt',$score)->find();
        //2、更改订单
        $aSnapItems = [
            'id' => null,
            'thumb' => null,
            'product_id'=>0,
            'title' => null,
            'content' => null,
        ];
            // 以服务器价格为准，生成订单
        $aSnapItems['id'] = $answer['id'];
        $aSnapItems['thumb'] = $answer['thumb'];
        $aSnapItems['product_id'] = $answer['product_id'];
        $aSnapItems['title'] = $answer['title'];
        $aSnapItems['snap_items'] = $answer['content'];
        //更新订单
        $res = OrderModel::where('id', $id)->update(['statustest' => 2,'snap_content' => $answer['content'],'snap_items'=>json_encode($aSnapItems)]);
        if ($res ) {
            //throw new MissException([
            //    'msg' => '请求订单不存在',
            //    'errorCode' => 40000
            //]);
            $one = OrderModel::where(['id'=>$id])->find();
            return $one;
        }
    }

    public function getAnswer2(){
        $id = input('get.id');//订单id
        $one = OrderModel::where(['id'=>$id])->find();
        return $one;
    }
}