<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:32
 */

namespace app\api\controller\v2;

use app\api\controller\BaseController;
//use app\api\service\AppToken;
use app\api\service\UserToken;
//use app\api\validate\AppTokenGet;
use app\api\validate\TokenGet;
use app\api\service\Token as TokenService;
use app\lib\exception\ParameterException;

/**
 * 令牌
 * Class Token
 * @package app\api\controller
 */
class Token{

    /**
     * 获取令牌
     * @param string $code
     * @return array
     */
    public function getToken($code=''){

        (new TokenGet())->goCheck();
        $wx = new UserToken($code);
        $token = $wx->get();
        return [
            'token' => $token
        ];
    }

    /**
     * 第三方应用获取令牌
     * @url /app_token?
     * @POST ac=:ac se=:secret
     */
    //public function getAppToken($ac='', $se=''){
    //    header('Access-Control-Allow-Origin: *');
    //    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    //    header('Access-Control-Allow-Methods: GET');
     //   (new AppTokenGet())->goCheck();
     //   $app = new AppToken();
    //    $token = $app->get($ac, $se);
   //     return [
   //         'token' => $token
   //     ];
    //}

    /**
     * 验证令牌
     * @param string $token
     * @return array
     * @throws ParameterException
     */
    public function verifyToken($token=''){
        if(!$token){
            throw new ParameterException([
                'token不允许为空'
            ]);
        }
        $valid = TokenService::verifyToken($token);
        return [
            'isValid' => $valid
        ];
    }

}