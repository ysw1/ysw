<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:32
 */

namespace app\api\controller\v2;

use app\api\controller\BaseController;
use app\api\service\Pay1 as PayService;
use app\api\service\WxNotify;
use app\api\validate\IDMustBePositiveInt;

class Pay extends BaseController{

    protected $beforeActionList = [
        'checkExclusiveScope' => ['only' => 'getPreOrder']
    ];

    //订单id
    public function getPreOrder($id=''){
        //(new IDMustBePositiveInt()) -> goCheck();
        $pay= new PayService();
        return $pay->pay($id);
    }

    //查询订单状态返回给头条
    public function checkOrderStatus($tradeno=''){
        $pay = new PayService();
        return $pay->checkOrderStatus($tradeno);
    }

    public function redirectNotify(){
        $notify = new WxNotify();
        $notify->handle();
    }

    public function notifyConcurrency(){
        $notify = new WxNotify();
        $notify->handle();
    }
    
    public function receiveNotify(){
//通知频率为15/30/180/1800/1800/1800/1800/3600       单位为秒
        //$xmlData = file_get_contents('php://input');
        //Log::error($xmlData);
        $notify = new WxNotify();
        $notify->handle();

        //$weixinData = file_get_contents("php://input");
        //file_put_contents('/tmp/2.txt', $weixinData, FILE_APPEND);





    }
}