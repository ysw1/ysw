<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:32
 */

namespace app\api\controller\v2;

use app\api\controller\BaseController;
use app\api\model\Category as CategoryModel;
use app\lib\exception\MissException;

class Category extends BaseController{

    /**
     * 获取全部类目列表，但不包含类目下的商品
     * Request 演示依赖注入Request对象
     * @url /category/all
     * @return array of Categories
     * @throws MissException
     */
    public function getAllCategories(){
        $categories = CategoryModel::all(['status'=>1]);
        if(empty($categories)){
            throw new MissException([
                'msg' => '还没有任何类目',
                'errorCode' => 50000
            ]);
        }
        foreach ($categories as $k=>&$v){
            $v['thumb'] = 'http://'. $_SERVER['SERVER_NAME'].$v['thumb'];
        }
        return $categories;
    }
}