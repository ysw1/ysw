<?php
/**
 * Created by WXCH
 * Date: 2020/1/10 0010 20:28
 */

namespace app\api\controller\v2;

use app\api\controller\BaseController;
use app\api\model\User as UserModel;
use app\api\service\Token as TokenService;
use app\lib\exception\InsertException;
use app\lib\exception\SuccessMessage;
use app\lib\exception\UserException;

class User extends BaseController{

    /**
     * 查询用户信息
     * @return \think\response\Json
     */
    public function queryUserInfo(){
        $uid = TokenService::getCurrentUid();
        if($uid){
            $user = UserModel::get($uid);
            return $user;
        }else{
            return json(new SuccessMessage(), 201);
        }
    }

    public function saveUserInfo(){
        $data = input('post.userInfo/a');
        $uid = TokenService::getCurrentUid();
        if($uid){
            // 存在则更新
            $user = UserModel::get($uid);
            if(!$user){
                $user = new UserModel();
                $user['id'] = $uid;
            }
            $data['gender'] = 1;
            $data['create_time'] = time();
            $result = $user->save($data);
            if($result){
                return new SuccessMessage();
            }else{
                return new InsertException();
            }

        }else{
            return new UserException(['msg'=>$uid]);
        }

    }

}