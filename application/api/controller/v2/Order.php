<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:32
 */

namespace app\api\controller\v2;

use app\api\controller\BaseController;

use app\api\model\Order as OrderModel;
use app\api\service\Order1 as OrderService;
use app\api\service\Token;
use app\api\validate\OrderValidate;


class Order extends BaseController{

    /**
     * 下单
     * @url /order
     * @HTTP POST
     */
    public function placeOrder(){
        (new OrderValidate())->goCheck();
        $product_id = input('post.product_id');
        $uid = Token::getCurrentUid();
        $order = new OrderService();
        $status = $order->place($uid, $product_id);
        return $status;
    }

    public function getOrderList(){
        $uid = Token::getCurrentUid();
        //全部订单
        //$statustest = 0;
        //$allOrderList = OrderModel::getOrderResultList($uid,$statustest);
        //待付
        $statustest = 1;
        $daiOrderList = OrderModel::getOrderList($uid,$statustest);

        //已付
        $statustest = 2;
        $overOrderList = OrderModel::getOrderList($uid,$statustest);
        return [
            // 'allList' => $allOrderList,
            'daiList' => $daiOrderList,
            'overList' => $overOrderList
        ];
    }
}