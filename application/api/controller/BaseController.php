<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:04
 */
namespace app\api\controller;

use app\api\service\Token;
use think\Controller;

class BaseController extends Controller{

    protected function checkExclusiveScope(){
        Token::needExclusiveScope();
    }

    protected function checkPrimaryScope(){
        Token::needPrimaryScope();
    }

    protected function checkSuperScope(){
        Token::needSuperScope();
    }

}