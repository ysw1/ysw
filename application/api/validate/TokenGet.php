<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 19:07
 */

namespace app\api\validate;

class TokenGet extends BaseValidate{

    protected $rule = [
        'code' => 'require|isNotEmpty'
    ];

    protected $message=[
        'code' => '没有code还想拿token？做梦哦'
    ];
}