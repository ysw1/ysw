<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 09:47
 */

namespace app\api\validate;

class IDMustBePositiveInt extends BaseValidate{
    protected $rule = [
        'id' => 'require|isPositiveInteger',
    ];
}
