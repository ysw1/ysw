<?php
/**
 * Created by WXCH
 * Date: 2020/1/13 0013 15:30
 */

namespace app\api\validate;

class OrderValidate extends BaseValidate{

    protected $rule = [
        'product_id' => 'require|isPositiveInteger'
    ];
}