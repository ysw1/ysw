<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:32
 */
namespace app\api\service;

use app\api\model\Cooperate as CooperateModel;
use think\Exception;

class Cooperate{

    public function place($uid, $cooperateInfo){

        try {
            $mCooperateModel = new CooperateModel();
            $order = $mCooperateModel->where(['user_id'=>$uid])->find();
            if($order){
                return [
                    'status' => 2,
                    'message' => '已经提交过不能再次提交!'
                ];
            }else {
                $mCooperateModel->user_id = $uid;
                $mCooperateModel->account = $cooperateInfo['account'];
                $mCooperateModel->num = $cooperateInfo['num'];
                $mCooperateModel->name = $cooperateInfo['name'];
                $mCooperateModel->contactinfo = $cooperateInfo['contactinfo'];
                $mCooperateModel->create_time = time();
                $mCooperateModel->save();
                return [
                    'status' => 1,
                    'message' => '提交成功!'
                ];
            }
        } catch (Exception $ex) {
            throw $ex;
        }

    }



}