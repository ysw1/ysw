<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:47
 */

namespace app\api\model;

class Product extends BaseModel{

    public function questions(){
        return $this->hasMany('Question','product_id');
    }
    public function answers(){
        return $this->hasMany('Answer','product_id');
    }

    public function getThumbAttr($value){
        return $this->prefixImageUrl($value);
    }
 	public function getThumbbannerAttr($value){
        return $this->prefixImageUrl($value);
    }
    public static function getOne($id){
        //return self::with(['questions','questions.options','answers'])->find($id);
        return self::with(['questions','questions.options'])->find($id);
    }

    public static function getHotProductList(){
        return  self::all(function($query){
            $query->where(['is_hot'=>1,'status'=>1])->order('sort desc');
        });
    }

    public static function getRecommendProductList(){
       return  self::all(function($query){
            $query->where(['is_recommend'=>1,'status'=>1])->order('sort desc');
        });
    }

    /**
     * 获取某分类下商品
     * @param $categoryID
     * @param int $page
     * @param int $size
     * @param bool $paginate
     * @return \think\Paginator
     */
    public static function getProductsByCategoryID($categoryID, $paginate = true, $page = 1, $size = 30){
        $query = self::where('category_id', '=', $categoryID)->where('status', '=',1)->order('sort desc');
        if (!$paginate) {
            return $query->select();
        } else {
            // paginate 第二参数true表示采用简洁模式，简洁模式不需要查询记录总数
            return $query->paginate(
                $size, true, [
                'page' => $page
            ]);
        }
    }
}
