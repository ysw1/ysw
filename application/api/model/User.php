<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:47
 */

namespace app\api\model;


class User extends BaseModel{

    protected $autoWriteTimestamp = true;

    /**
     * 用户是否存在
     * 存在返回uid，不存在返回0
     */
    public static function getByOpenID($openid){
        $user = User::where('openid', '=', $openid)->find();
        return $user;
    }
}