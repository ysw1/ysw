<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:47
 */

namespace app\api\model;

class Question extends BaseModel{

    public function options(){
        return $this->hasMany('Option','question_id');
    }

}
