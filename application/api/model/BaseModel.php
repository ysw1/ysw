<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:45
 */

namespace app\api\model;

use think\Model;
use traits\model\SoftDelete;

class BaseModel extends Model{

    use SoftDelete;

    protected $hidden = ["delete_time"];

    protected $insert = ['create_time'];

    protected $update = ['update_time'];

    protected function setCreateTimeAttr(){
        return time();
    }

    protected function setUpdateTimeAttr(){
        return time();
    }

    protected function prefixImageUrl($value){
        return config("setting.domain_prefix").$value;
    }

}