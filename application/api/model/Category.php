<?php
namespace app\api\model;

class Category extends BaseModel{
    public function products(){
        return $this->hasMany('Product', 'category_id', 'id');
    }

    public static function getCategories($ids){
        $categories = self::with('products')->select($ids);
        return $categories;
    }

}
