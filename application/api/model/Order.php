<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:47
 */

namespace app\api\model;


class Order extends BaseModel{

    public static function getOrderList($user_id,$statustest){
        if($statustest == 1){
            return self::all([
                'user_id'=>$user_id,
                'statustest'=>$statustest
            ]);
        }elseif($statustest == 2){
            return self::all([
                'user_id'=>$user_id,
                'statustest'=>$statustest
            ]);
        }else{
            return self::all([
                'user_id'=>$user_id
            ]);
        }

    }

}
