<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:47
 */

namespace app\api\model;


class Banner extends BaseModel{

    public function getThumbAttr($value){
        return $this->prefixImageUrl($value);
    }

    public static function getBannerList(){
        return  self::all();
    }
}
