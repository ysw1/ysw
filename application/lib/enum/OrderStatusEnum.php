<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:32
 */
namespace app\lib\enum;

class OrderStatusEnum{

    // 待支付
    const UNPAID = 1;

    // 已支付
    const PAID = 2;

    // 已处理PAID_BUT_OUT_OF
    const HANDLED_OUT_OF = 5;
}