<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:15
 */

namespace app\lib\enum;

class ScopeEnum{

    //api权限
    const User = 16;

    //后台管理员权限
    const Super = 32;

}