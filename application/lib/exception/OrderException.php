<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:19
 */

namespace app\lib\exception;

class OrderException extends BaseException{
    public $code = 404;
    public $msg = '订单不存在，请检查ID';
    public $errorCode = 80000;
}