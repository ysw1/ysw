<?php
/**
 * Created by WXCH
 * Date: 2020/1/12 0012 15:01
 */

namespace app\lib\exception;

class UserException extends BaseException{
    public $code = 404;
    public $msg = '用户不存在';
    public $errorCode = 60000;
}