<?php
/**
 * Created by WXCH
 * Date: 2020/1/10 0010 20:31
 */

namespace app\lib\exception;


class ThemeException extends BaseException
{
    public $code = 404;
    public $msg = '指定主题不存在，请检查主题ID';
    public $errorCode = 30000;
}