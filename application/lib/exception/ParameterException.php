<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 09:44
 */

namespace app\lib\exception;

/**
 * 通用参数类异常错误
 */
class ParameterException extends BaseException
{
    /**
     * 状态码
     */
    public $code = 400;

    /**
     * 错误码
     */
    public $errorCode = 10000;

    /**
     * 错误具体信息描述
     */
    public $msg = "invalid parameters";
}