<?php
/**
 * Created by WXCH
 * Date: 2020/1/12 0012 15:01
 */

namespace app\lib\exception;

class InsertException extends BaseException{
    public $code = 404;
    public $msg = '插入数据失败';
    public $errorCode = 60001;
}