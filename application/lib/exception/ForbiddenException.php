<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:19
 */

namespace app\lib\exception;

/**
 * token验证失败时抛出异常
 * Class ForbiddenException
 * @package app\lib\exception
 */
class ForbiddenException extends BaseException
{

    public $code = 403;
    public $msg = "权限不够";
    public $errorCode = 10001;

}