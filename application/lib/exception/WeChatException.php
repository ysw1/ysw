<?php
/**
 * Created by WXCH
 * Date: 2020/1/8 0008 18:21
 */

namespace app\lib\exception;

/**
 * 微信服务器异常
 */
class WeChatException extends BaseException
{
    public $code = 400;
    public $msg = 'wechat unknown error';
    public $errorCode = 999;
}