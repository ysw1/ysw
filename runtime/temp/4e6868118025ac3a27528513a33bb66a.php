<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:81:"D:\phpstudy_pro\WWW\psychology\public/../application/admin\view\category\add.html";i:1592535647;s:70:"D:\phpstudy_pro\WWW\psychology\application\admin\view\common\base.html";i:1592389065;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>心理测评集后台管理系统</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/admin/layuimini/lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/layuimini/css/public.css" media="all">

</head>
<body>
<!-- 页面中的内容 -->

<div class="layui-form layuimini-form">
    <div class="layui-form-item">
        <label class="layui-form-label required">类别名称</label>
        <div class="layui-input-block">
            <input type="text" name="name" lay-verify="required" lay-reqtext="类别名称不能为空" placeholder="请输入类别名称" value="" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">封面图</label>
        <div class="layui-input-block">
            <button type="button" class="layui-btn" id="upload_btn">
                <i class="layui-icon">&#xe67c;</i>上传图片
            </button>
            <div><tip>上传图片正方形</tip></div>
            <img id="previewImg" width="60px" height="60px">
            <input id="upload_image" name="thumb" type="hidden" multiple="true" value="">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">状态</label>
        <div class="layui-input-block">
            <input type="radio" name="status" value="0" title="禁用" checked="">
            <input type="radio" name="status" value="1" title="开启">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">排序</label>
        <div class="layui-input-block">
            <input type="number" name="sort" placeholder="请输入排序" value="" class="layui-input">
            <tip>只能输入数字!</tip>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="saveBtn">确认保存</button>
        </div>
    </div>
</div>
<script src="/static/admin/layuimini/lib/jquery-3.4.1/jquery-3.4.1.min.js" charset="utf-8"></script>
<script src="/static/admin/layuimini/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script src="/static/admin/layuimini/js/commons.js" charset="utf-8"></script>
<script src="/static/admin/layuimini/js/image.js" charset="utf-8"></script>
<script type="text/javascript">
    var url = '/admin/Category/add';
    add_edit(url);
    image_upload();
</script>

</body>
</html>