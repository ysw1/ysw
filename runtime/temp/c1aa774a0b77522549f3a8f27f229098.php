<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:80:"D:\phpstudy_pro\WWW\psychology\public/../application/admin\view\product\lst.html";i:1592389065;s:70:"D:\phpstudy_pro\WWW\psychology\application\admin\view\common\base.html";i:1592389065;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>心理测评集后台管理系统</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/admin/layuimini/lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/layuimini/css/public.css" media="all">

</head>
<body>
<!-- 页面中的内容 -->

<div class="layuimini-container">
    <div class="layuimini-main">
        <div class="layui-btn-container">
            <button class="layui-btn layui-btn-sm data-add-btn"> 新增商品 </button>
        </div>
        <div class="layui-form" style="margin-top: 20px;">
            <table class="layui-table" style="table-layout: fixed;">
                <colgroup>
                    <col width="40">
                    <col width="70">
                    <col width="250">
                    <col width="100">
                    <col width="120">
                    <col width="120">
                    <col width="70">
                    <col width="70">
                    <col width="80">
                    <col width="80">
                    <col width="80">
                    <col width="200">
                    <col width="250">
                </colgroup>
                <thead>
                <tr>
                    <th>ID</th>
                    <th>排序</th>
                    <th>商品名</th>
                    <th>类目</th>
                    <th>封面图</th>
                    <th>内页banner图</th>
                    <th>价格￥</th>
                    <th>浏览量(假)</th>
                    <th>推荐</th>
                    <th>热门</th>
                    <th>状态</th>
                    <th>创建时间</th>
                    <th>操作管理</th>
                </tr>
                </thead>
                <tbody>
                <?php if(is_array($list['data']) || $list['data'] instanceof \think\Collection || $list['data'] instanceof \think\Paginator): $i = 0; $__LIST__ = $list['data'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                <tr>
                    <td><?php echo $vo['id']; ?></td>
                    <td>
                        <div class="layui-input-inline">
                            <input type="text" value="<?php echo $vo['sort']; ?>" data-id="<?php echo $vo['id']; ?>" class="changeSort layui-input">
                        </div>
                    </td>
                    <td><?php echo $vo['name']; ?></td>
                    <td><?php echo $vo['category']['name']; ?></td>
                    <td><img src="<?php echo $vo['thumb']; ?>"></td>
                    <td><img src="<?php echo $vo['thumbbanner']; ?>"></td>
                    <td><?php echo $vo['price']; ?></td>
                    <td><?php echo $vo['fakenum']; ?></td>
                    <td data-id="<?php echo $vo['id']; ?>">
                        <input type="checkbox" <?php if($vo['is_recommend'] == 1): ?>checked<?php endif; ?> name="is_recommend" lay-skin="switch" lay-filter="switchRecommend" lay-text="ON|OFF">
                    </td>
                    <td data-id="<?php echo $vo['id']; ?>">
                        <input type="checkbox" <?php if($vo['is_hot'] == 1): ?>checked<?php endif; ?> name="is_hot" lay-skin="switch" lay-filter="switchHot" lay-text="ON|OFF">
                    </td>
                    <td data-id="<?php echo $vo['id']; ?>">
                        <input type="checkbox" <?php if($vo['status'] == 1): ?>checked<?php endif; ?> name="status" lay-skin="switch" lay-filter="switchStatus" lay-text="ON|OFF">
                    </td>
                    <td><?php echo $vo['create_time']; ?></td>
                    <td>
                        <a class="layui-btn layui-btn-xs data-count-edit questionSetting" lay-event="questionSetting" data-id="<?php echo $vo['id']; ?>">问题设置</a>
                        <a class="layui-btn layui-btn-xs data-count-edit answerSetting" lay-event="answerSetting" data-id="<?php echo $vo['id']; ?>">答案设置</a>
                        <a class="layui-btn layui-btn-xs data-count-edit edit" lay-event="edit" data-id="<?php echo $vo['id']; ?>">编辑</a>
                        <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete delete" lay-event="delete" data-id="<?php echo $vo['id']; ?>">删除</a>
                    </td>
                </tr>
                <?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
        </div>
    </div>

    <div id="pages"></div>
</div>
<script src="/static/admin/layuimini/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>

<script>

    layui.use(['form', 'table', 'layer', 'laypage'], function () {
        var $ = layui.jquery,
            form = layui.form,
            layer = layui.layer,
            laypage = layui.laypage;
            laypage.render({ //分页
                elem: 'pages',
                count: '<?php echo $list["total"]; ?>',  // 新加的内容哦。
                theme: '#FFB800',
                //, curr: param['page']
                curr: '<?php echo $list["current_page"]; ?>', // 完美解决哦。
                //,hash: 'page' //自定义hash值
                jump: function(obj, first){
                    //obj包含了当前分页的所有参数，比如：
                    //首次不执行
                    if(!first){
                        //do something
                        location.href="?page="+obj.curr
                    }
                }
            });
        // 监听添加操作
        $(".data-add-btn").on("click", function () {
            var index = layer.open({
                title: '添加商品',
                type: 2,
                shade: 0.2,
                maxmin:true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '<?php echo url("product/add"); ?>'
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;
        });
        $('.changeSort').on('change',function () {
            var id = $(this).attr('data-id');
            var sort = $(this).val();
            if(!sort){
                return;
            }
            $.ajax({
                url: '<?php echo url("product/sort"); ?>?id=' + id + '&sort=' + sort,
                success: function(res){
                    if(res.status == 0) {
                        window.location.reload();
                    }
                    layer.msg(res.message);
                }
            });
        });
        $('.questionSetting').on('click', function () {
            var id = $(this).attr('data-id'); // 商品id
            var index = layer.open({
                title: '问题设置',
                type: 2,
                shade: 0.2,
                maxmin:true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '<?php echo url("question/lst"); ?>?product_id=' + id
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
        });
        $('.answerSetting').on('click', function () {
            var id = $(this).attr('data-id'); // fu
            var index = layer.open({
                title: '答案设置',
                type: 2,
                shade: 0.2,
                maxmin:true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '<?php echo url("answer/lst"); ?>?product_id=' + id
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
        });
        $('.edit').on('click', function () {
            var id = $(this).attr('data-id'); // fu
            var index = layer.open({
                title: '编辑商品',
                type: 2,
                shade: 0.2,
                maxmin:true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '<?php echo url("product/edit"); ?>?id=' + id
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
        });
        $('.delete').on('click', function () {
            var id = $(this).attr('data-id'); // fu
            layer.confirm('真的删除么?', function (index) {
                $.ajax({
                    url: '<?php echo url("product/del"); ?>?id=' + id,
                    type: 'GET',
                    success: function(res) {
                        window.location.reload();
                        layer.msg(res.msg, {
                            icon: 1,
                            time: 1000 //2秒关闭（如果不配置，默认是3秒）
                        }, function(){
                            layer.close(index);
                        });
                    },
                    error: function(res) {
                        //return   err(res)
                    }
                });
            });
        });

        //监听状态 更改
        form.on('switch(switchRecommend)', function (obj) {
            var id = obj.othis.parent().attr('data-id');
            var recommend = obj.elem.checked ? 1 : 0;
            $.ajax({
                url: '<?php echo url("product/recommend"); ?>?id=' + id + '&recommend=' + recommend,
                success: function(res){
                    if(res.status == 0) {
                        window.location.reload();
                    } else {
                        layer.msg(res.message);
                    }
                }
            });
            return false;
        });

        //监听状态 更改
        form.on('switch(switchHot)', function (obj) {
            var id = obj.othis.parent().attr('data-id');
            var hot = obj.elem.checked ? 1 : 0;
            $.ajax({
                url: '<?php echo url("product/hot"); ?>?id=' + id + '&hot=' + hot,
                success: function(res){
                    if(res.status == 0) {
                        window.location.reload();
                    }
                    layer.tips(res.message)
                }
            });
            return false;
        });

        //监听状态 更改
        form.on('switch(switchStatus)', function (obj) {
            var id = obj.othis.parent().attr('data-id');
            var status = obj.elem.checked ? 1 : 0;
            $.ajax({
                url: '<?php echo url("product/status"); ?>?id=' + id + '&status=' + status,
                success: function(res){
                    if(res.status == 0) {
                        window.location.reload();
                    } else {
                        layer.msg(res.message);
                    }
                }
            });
            return false;
        });
    });
</script>

</body>
</html>